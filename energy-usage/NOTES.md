Candidate Name: Piotr Jablonski

Tasks: 1, 3

Time: ~3h

Notes:

File / code structure inspired by [“fractals" approach](https://hackernoon.com/fractal-a-react-app-structure-for-infinite-scale-4dab943092af). Each component has main file `Component1.jsx` and may have a folder `component1` containing subcomponents and utilities used only by `Component1`. This structure is very practical and scalable.
Components that can potentially be shared are in src/components folder.

I choose  ‘date-fns’ library instead of ‘moment’. As you well spotted in README file, momentjs requires workarounds and is not a good fit for an environment where data immutability is important.