import React from 'react';
import MeterReadingsTable from './dashboard/MeterReadingsTable';
import utils from './dashboard/utils';
import Chart from './dashboard/Chart';
import Header from '../components/Header';

// eslint-disable-next-line react/prefer-stateless-function
export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { meterReadingsData: null, isLoading: true };
  }

  componentDidMount() {
    utils.fetchData().then((newState) => {
      this.setState(newState);
    });
  }

  render() {
    const { isLoading } = this.state;
    const meterReadings = utils.selectMeterReadings(this.state);

    if (isLoading) {
      return 'loading data...';
    }

    const estimatedMeterReadings = utils.estimateAllReadings(meterReadings);
    return (
      <div>
        <Header>Energy Usage</Header>
        <Chart energyUsageData={estimatedMeterReadings} />
        <Header>Meter Readings</Header>
        <MeterReadingsTable meterReadings={meterReadings} />
      </div>
    );
  }
}
