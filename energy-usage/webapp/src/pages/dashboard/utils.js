import * as fns from 'date-fns';

const dataUrl = 'https://storage.googleapis.com/bulb-interview/meterReadingsReal.json';

const fetchData = () => fetch(dataUrl)
  .then(res => res.json())
  .then(meterReadingsData => ({ isLoading: false, meterReadingsData }))
  .catch(() => Promise.resolve({ isLoading: false, meterReadingsData: null }));

const selectMeterReadings = ({ meterReadingsData }) =>
  (meterReadingsData ? meterReadingsData.electricity || null : null);

const estimateReadingAtTheEndOfMonth = ([r1, r2]) => {
  if (!r1 || !r2 || !fns.isValid(r1.readingDate) || !fns.isValid(r2.readingDate)) {
    return null;
  }

  if (fns.isLastDayOfMonth(r1.readingDate)) {
    return r1.cumulative;
  }

  const intervalLengthInDays = Math.abs(fns.differenceInDays(r1.readingDate, r2.readingDate));
  const usageInInterval = Math.abs(r2.cumulative - r1.cumulative);

  const estimatedUsagePerDay = usageInInterval / intervalLengthInDays;
  const lastDayOfMonth = fns.lastDayOfMonth(r1.readingDate);
  const daysUntlEndOfMonth = Math.abs(fns.differenceInCalendarDays(r1.readingDate, lastDayOfMonth));
  const result = r1.cumulative + (estimatedUsagePerDay * daysUntlEndOfMonth);
  return result;
};

const estimateAllReadings = (readings) => {
  if (!readings) return [];
  const res = readings.reduce((acc, val, i, obj) => {
    const realReadings = [val, obj[i + 1]];
    const cumulative = estimateReadingAtTheEndOfMonth(realReadings);
    if (!cumulative) return acc;
    const readingDate = fns.format(fns.lastDayOfMonth(val.readingDate), 'YYYY-MM-dd\'T\'HH:mm:ss.SSSZ');
    const usage = i > 0 ? Math.abs(acc[i - 1].cumulative - cumulative) : null;
    return acc.concat({
      cumulative,
      readingDate,
      usage,
    });
  }, []);
  return res;
};

export default {
  fetchData,
  selectMeterReadings,
  estimateReadingAtTheEndOfMonth,
  estimateAllReadings,
};
