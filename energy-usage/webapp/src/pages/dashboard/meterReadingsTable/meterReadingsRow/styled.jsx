import styled from 'styled-components';

/* eslint-disable import/prefer-default-export */
export const Col = styled.td`
  padding: 10px;
  border: solid 1px lightgray;
  text-align: center;
`;
