import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import MeterReadingRow from './MeterReadingsRow';
import { Col } from './meterReadingsRow/styled';

const reading = {
  cumulative: 17600,
  readingDate: '2017-03-31T00:00:00.000Z',
  unit: 'kWh',
};

describe('component MeterReadingRow', () => {
  it('renders a single row', () => {
    const actual = shallow(<MeterReadingRow reading={reading} />).exists('tr');
    expect(actual).toBeTruthy();
  });
  it('contains 3 columns', () => {
    const actual = mount(<MeterReadingRow reading={reading} />).find(Col).map(elem => elem.text());
    const expected = ['2017-03-31T00:00:00.000Z', '17600', 'kWh'];
    expect(actual).toEqual(expected);
  });
});

test('snapshot', () => {
  const tree = renderer
    .create(<MeterReadingRow reading={reading} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
