import styled from 'styled-components';

/* eslint-disable import/prefer-default-export */
export const Table = styled.table`
border-collapse: collapse;
width: 50%;
margin: 20px auto;
table-layout: fixed;
`;
