import React from 'react';
import { Col } from './meterReadingsRow/styled';

export default ({ reading }) => (
  <tr key={reading.readingDate}>
    <Col>{reading.readingDate}</Col>
    <Col>{reading.cumulative}</Col>
    <Col>{reading.unit}</Col>
  </tr>
);
