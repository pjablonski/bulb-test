import React from 'react';
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

export default ({ energyUsageData }) =>
  (
    <BarChart width={1400} height={400} data={energyUsageData}>
      <XAxis dataKey="readingDate" />
      <YAxis dataKey="usage" />
      <CartesianGrid horizontal={false} />
      <Tooltip />
      <Bar dataKey="usage" fill="#03ad54" isAnimationActive={false} />
    </BarChart>
  );
