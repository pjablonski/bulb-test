import utils from './utils';

const mockFetch = data => jest.fn().mockImplementation(() =>
  Promise.resolve({
    ok: true,
    json: () => data,
  }));

const mockFetchFailed = () => jest.fn().mockImplementation(() =>
  Promise.reject());

describe('fetchData', () => {
  it('should return non loading state with data if data was fetched correctly', async () => {
    const data = [{}, {}];
    global.fetch = mockFetch(data);
    const actual = await utils.fetchData('');
    const expected = { isLoading: false, meterReadingsData: data };
    expect(actual).toEqual(expected);
  });

  it('should return non loading state with empty data if there was an erroe', async () => {
    global.fetch = mockFetchFailed();
    const actual = await utils.fetchData('');
    const expected = { isLoading: false, meterReadingsData: null };
    expect(actual).toEqual(expected);
  });
});

describe('estimateReadingAtTheEndOfMonth', () => {
  const r1 = {
    cumulative: 17100,
    readingDate: '2017-01-21T00:00:00.000Z',
  };

  const r2 = {
    cumulative: 17300,
    readingDate: '2017-02-10T00:00:00.000Z',
  };

  it('should return null for edge cases', () => {
    const actual = utils.estimateReadingAtTheEndOfMonth([r1, null]);
    expect(actual).toBeNull();
  });

  it('should return null for edge cases', () => {
    const actual = utils.estimateReadingAtTheEndOfMonth([null, r2]);
    expect(actual).toBeNull();
  });

  it('should return cumulative value if the readining was taken at the last day of a month', () => {
    const actual = utils.estimateReadingAtTheEndOfMonth([{
      ...r1,
      readingDate: '2017-01-31T00:00:00.000Z',
    }, r2]);
    expect(actual).toEqual(17100);
  });


  it('should calculate diff', () => {
    const actual = utils.estimateReadingAtTheEndOfMonth([r1, r2]);
    expect(actual).toEqual(17200);
  });
});


describe('estimateAllReadings', () => {
  const r1 = {
    cumulative: 17100,
    readingDate: '2018-01-21T00:00:00.000Z',
  };

  const r2 = {
    cumulative: 17300,
    readingDate: '2018-02-10T00:00:00.000Z',
  };

  const r3 = {
    cumulative: 17400,
    readingDate: '2018-03-02T00:00:00.000Z',
  };

  it('should return array of estimated readings', () => {
    const actual = utils.estimateAllReadings([r1, r2, r3]);
    const expected = [
      {
        cumulative: 17200,
        readingDate: '2018-01-31T00:00:00.000Z',
        usage: null,
      },
      {
        cumulative: 17390,
        readingDate: '2018-02-28T00:00:00.000Z',
        usage: 190,
      },
    ];
    expect(actual).toEqual(expected);
  });
});
