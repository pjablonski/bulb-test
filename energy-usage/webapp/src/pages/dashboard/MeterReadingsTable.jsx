import React from 'react';
import MeterReadingsRow from './meterReadingsTable/MeterReadingsRow';
import { Table } from './meterReadingsTable/styled';

export default ({ meterReadings }) => (
  <Table>
    <thead>
      <tr>
        <th>Date</th>
        <th>Reading</th>
        <th>Unit</th>
      </tr>
    </thead>
    <tbody>
      {meterReadings.map(reading => (
        <MeterReadingsRow key={reading.readingDate} reading={reading} />
      ))}
    </tbody>
  </Table>
);
