import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import MeterReadingTable from './MeterReadingsTable';
import MeterReadingRow from './meterReadingsTable/MeterReadingsRow';

const meterReadings = [
  {
    cumulative: 17600,
    readingDate: '2017-03-31T00:00:00.000Z',
    unit: 'kWh',
  },
  {
    cumulative: 17859,
    readingDate: '2017-04-30T00:00:00.000Z',
    unit: 'kWh',
  },
  {
    cumulative: 18102,
    readingDate: '2017-05-31T00:00:00.000Z',
    unit: 'kWh',
  },
];

const genShallow = () =>
  shallow(<MeterReadingTable meterReadings={meterReadings} />);

describe('component MeterReadingTable', () => {
  it('renders header', () => {
    const actual = genShallow().exists('thead');
    expect(actual).toBe(true);
  });
  it('contains 3 rows', () => {
    const actual = genShallow().find(MeterReadingRow).length;
    const expected = 3;
    expect(actual).toEqual(expected);
  });
});

test('snapshot', () => {
  const tree = renderer
    .create(<MeterReadingTable meterReadings={meterReadings} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
