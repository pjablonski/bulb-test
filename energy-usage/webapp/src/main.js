/* eslint-env browser */

import React from 'react';
import ReactDOM from 'react-dom';

import Dashboard from './pages/Dashboard';


document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    React.createElement(Dashboard),
    document.getElementById('mount'),
  );
});
